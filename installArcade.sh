#/bin/bash

# gunicorn listens on ip4, midori calls locahost on ip6. Let's disable ip6
echo net.ipv6.conf.all.disable_ipv6=1 > /etc/sysctl.d/disableipv6.conf

#get the default username and home path
read defaultuser defaultuserhome <<< $(awk -F":" '$3=="1000" {print $1, $6}' /etc/passwd)

echo "** Install packages"
apt-get update && apt-get -y upgrade 
apt-get -y install sudo ssh xinit openbox lxterminal git curl python-pip python-virtualenv midori chromium
apt-get clean


## install arcade server software
sudo $defaultuser
cd ~
virtualenv venv
source ~/venv/bin/activate
pip install flask lxml gunicorn

git clone https://gitlab.com/Arcade-DIWO/project-manager.git arcade
git clone https://gitlab.com/Arcade-DIWO/common-projects.git arcade/projects/common
mkdir ~/arcade/projects/local
ln -s ~/arcade/projects/ ~/arcade/server/static/

git clone https://github.com/jmoenig/Snap--Build-Your-Own-Blocks arcade/snap
ln -s ~/arcade/snap/ ~/arcade/server/static/


## configure openbox
mkdir -p $defaultuserhome/.config/openbox
cp -r /etc/xdg/openbox/* $defaultuserhome/.config/openbox/
chown -R $defaultuser $defaultuserhome/.config/openbox 

vim $defaultuserhome/.config/openbox/rc.xml
--------------------8<-----------------
<openbox_config xmlns="http://openbox.org/3.4/rc" xmlns:xi="http://www.w3.org/2001/XInclude">
    <keyboard>
    <keybind key="F8">
    <action name="Execute"><command>~/arcade/bash/kill-project</command></action>
    </keybind>
    </keyboard>
</openbox_config>
--------------------8<-----------------

# autostart gunicorn and midori
vim $defaultuserhome/.config/openbox/autostart
~/arcade/bash/start-server &
sleep 2
/usr/bin/midori -e Fullscreen -a http://localhost:8000 &
 

Note!!
Edit defaultuserhome/arcade/server/config.cfg

## autologin and hide mouse 
apt-get install lightdm
edit /etc/lightdm/lightdm.conf
--------------------8<-----------------
[Seat:*]
xserver-command =X -nocursor
autologin-user= arcade
--------------------8<-----------------

# update our server software and snap project repos

vim /etc/systemd/system/arcade-update.service
--------------------8<-----------------
[Unit]
Description=Update Arcade
Wants=network-online.target
After=network.target network-online.target

[Service]
ExecStart=/home/arcade/arcade/bash/update.sh

[Install]
WantedBy=multi-user.target
--------------------8<-----------------

systemctl enable arcade-update.service



#enable rc.local
#https://www.linuxbabe.com/linux-server/how-to-enable-etcrc-local-with-systemd
